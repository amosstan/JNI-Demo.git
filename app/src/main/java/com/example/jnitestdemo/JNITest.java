package com.example.jnitestdemo;

import android.util.Log;

public class JNITest {

    // Log tag
    private static final String LOG_TAG = "JNITest";

    private static MainView mView = null;

    static {
        System.loadLibrary("jni_test"); // 加载jni_test库
    }

    public JNITest(MainView mainView) {
        this.mView = mainView;
    }

    /* C层调用Java层 */
    public int CtoJavaReturn(int type) {
        debug("CtoJavaReturn: " + type);
        MainView view = JNITest.mView;
        mView.updateTextViewCtoJava(type);

        return 0;
    }

    /* Java调用C层 */
    public native String javaToC();
    public native String CtoJava();


    /* debug */
    private void debug(String info) {
        Log.d(LOG_TAG, info);
    }
}
