package com.example.jnitestdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MainView{

    // Log tag
    private static final String LOG_TAG = "JNITestDemo";

    // TextView
    private TextView mTextViewJavatoC = null;
    private TextView mTextViewCtoJava = null;

    // msg.what
    private static final int UPDATE_UI_JAVA_TO_C = 0x01;
    private static final int UPDATE_UI_C_TO_JAVA = 0x02;

    // UIHandler
    private UIHandler mUIHandler = null;

    // JNITest
    private static JNITest mJniTest = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        debug("onCreate");

        initView();
        initDataSource();
    }

    /* initView */
    private void initView() {
        mTextViewJavatoC = findViewById(R.id.textview_java_to_c);
        mTextViewCtoJava = findViewById(R.id.textview_c_to_java);
    }

    /* initDataSource */
    private void initDataSource() {
        mUIHandler = new UIHandler();
        mJniTest = new JNITest(this);
    }

    /* jniCallJavaToC */
    private void jniCallJavaToC() {
        String text = mJniTest.javaToC();
        debug("jniCallJavaToC: " + text);

        Message message = new Message();
        message.what = UPDATE_UI_JAVA_TO_C;
        message.obj = text;
        mUIHandler.sendMessage(message);
    }

    /* jniCallCToJava */
    private void jniCallCToJava() {
        mJniTest.CtoJava();
    }

    /* Button onClick */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_java_to_c:
                debug("click java to c");
                jniCallJavaToC();
                break;
            case R.id.button_c_to_java:
                debug("click c to java");
                jniCallCToJava();
                break;
                default:
                    break;
        }
    }

    @Override
    public void updateTextViewCtoJava(int info) {
        Message message = new Message();
        message.what = UPDATE_UI_C_TO_JAVA;
        message.arg1 = info;
        mUIHandler.sendMessage(message);
    }

    /* UIHandler */
    @SuppressLint("HandlerLeak")
    private class UIHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATE_UI_JAVA_TO_C:
                    mTextViewJavatoC.setText(String.valueOf(msg.obj));
                    break;
                case UPDATE_UI_C_TO_JAVA:
                    mTextViewCtoJava.setText(String.valueOf(msg.arg1));
                    break;
                    default:
                        break;
            }
        }
    }

    /* debug */
    private void debug(String info) {
        Log.d(LOG_TAG, info);
    }
}
